extern crate uuid;
extern crate clipboard;

use uuid::Uuid;
use clipboard::ClipboardProvider;
use clipboard::ClipboardContext;

fn main() {
    let guid = get_hyphen_uuid();

    let mut ctx: ClipboardContext = ClipboardProvider::new().unwrap();
    ctx.set_contents(guid.to_owned()).unwrap();

    println!("Copying GUID to clipboard {}", guid);
}

fn get_hyphen_uuid() -> String {
    let my_uuid = Uuid::new_v4();

    my_uuid.hyphenated().to_string()
}


